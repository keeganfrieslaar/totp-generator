const Express = require("express");
const BodyParser = require("body-parser");
const Speakeasy = require("speakeasy");
var cors = require("cors");

var app = Express();
app.use(cors());

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

app.post("/totp-secret", (request, response, next) => {
  var secret = Speakeasy.generateSecret({ length: 20 });
  response.send({ secret: secret.base32 });
});

app.post("/totp-generate", (request, response, next) => {
  response.send(
    Speakeasy.totp({
      secret: request.body.secret,
      encoding: "base32",
    })
    // remaining: 30 - Math.floor((new Date().getTime() / 1000.0) % 30)
  );
});

// window of 0 implicitly will be 30 seconds

app.post("/totp-validate", (request, response, next) => {
  response.send({
    valid: Speakeasy.totp.verify({
      secret: request.body.secret,
      encoding: "base32",
      token: request.body.token,
      window: 2,
    }),
  });
});

app.listen(3020, () => {
  console.log("Listening at :3020...");
});
